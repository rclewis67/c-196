package com.example.plainolnotes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBTermsOpenHelper extends SQLiteOpenHelper{


    //Constants for TERMS db name and version
    private static final String TERMS_DATABASE_NAME = "terms.db";
    private static final int TERMS_DATABASE_VERSION = 1;


    // TERMS variables
    public static final String TABLE_TERMS = "terms";
    public static final String TERM_ID = "_id";
    public static final String TERMS_TEXT = "termsText";
    public static final String TERMS_CREATED_DATE = "termCreated";

    // TERMS table
    public static final String[] TERMS_ALL_COLUMNS =
            {TERM_ID, TERMS_TEXT, TERMS_CREATED_DATE};

    // COURSES table
//    public static final String[] COURSES_ALL_COLUMNS =
//            {TERM_ID, TERMS_TEXT, TERMS_CREATED};

    //SQL to create table
    private static final String TERMS_TABLE_CREATE =
            "CREATE TABLE " + TABLE_TERMS + " (" +
                    TERM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    TERMS_TEXT + " TEXT, " +
                    TERMS_CREATED_DATE + " TEXT default CURRENT_TIMESTAMP" +
                    ")";


// -------------------------
    public DBTermsOpenHelper(Context context) {
        super(context, TERMS_DATABASE_NAME, null, TERMS_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TERMS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TERMS);
        onCreate(db);
    }

//    // testing TERMS
//    private static final String TERMS_TABLE_CREATE =
//            "CREATE TABLE " + TABLE_TERMS + " (" +
//                    TERM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
//                    TERMS_TEXT + " TEXT, " +
//                    TERMS_CREATED_DATE + " TEXT default CURRENT_TIMESTAMP" +
//                    ")";

//    public DBOpenTermsHelper(Context context) {
//        super(context, TERMS_DATABASE_NAME, null, TERMS_DATABASE_VERSION);
//    }

//    @Override
//    public void onTermsCreate(SQLiteDatabase db) {
//        db.execSQL(TERMS_TABLE_CREATE);
//    }
//
//    @Override
//    public void onTermsUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TERMS);
//        onCreate(db);
//    }

    // *************************************************************
    // need to make a new class called DBOpenTermsHelper and copy all of this
    // into it. Then delete the terms stuff from this file and the notes stuff
    // from the new file.
    // *************************************************************
}
