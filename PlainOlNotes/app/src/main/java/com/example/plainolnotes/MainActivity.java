package com.example.plainolnotes;
import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.support.v7.app.ActionBarActivity;

public class MainActivity extends ActionBarActivity {

    private Button btnLaunchTerms;
    private Button btnLaunchCourses;
    private Button btnLaunchAssessments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLaunchTerms = (Button) findViewById(R.id.btn_launch_terms);

        btnLaunchTerms.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) { launchTerms(); }
        });


        btnLaunchCourses = (Button) findViewById(R.id.btn_launch_courses);

        btnLaunchCourses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchCourses(); }
        });


        btnLaunchAssessments = (Button) findViewById(R.id.btn_launch_assessments);

        btnLaunchAssessments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { launchAssessments(); }
        });


    }

    private void launchTerms() {
        Intent intent = new Intent(this, Terms.class);
        startActivity(intent);
    }

    private void launchCourses() {
        Intent intent = new Intent(this, Courses.class);
        startActivity(intent);
    }
    private void launchAssessments() {
        Intent intent = new Intent(this, Assessments.class);
        startActivity(intent);
    }
}
